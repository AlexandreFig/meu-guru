/*
* DEFININDO O NOME DO VIRTUALHOST PARA SER CRIADO AUTOMATICAMENTE
* nomedoprojeto.rev
*/
const host = 'meuguru.local';

/*
* DEFININDO OS SCRIPTS E OS ARQUIVOS CSS QUE SERÃO UTILIZADOS NO PROJETO
*/
const scripts = [
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/jquery.mmenu/dist/jquery.mmenu.all.js',
    'node_modules/flickity/dist/flickity.pkgd.min.js',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    'node_modules/sweetalert/dist/sweetalert.min.js',
    'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
    'base/js/gallery.js',
    'base/js/main.js'
];

const styles  = [
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/jquery.mmenu/dist/jquery.mmenu.all.css',
    'node_modules/flickity/dist/flickity.min.css',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
    'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
    'node_modules/sweetalert/dist/sweetalert.css',
    'base/css/styles.css',
];

const watchList = [
    'fuel/app/views/frontend/*.php',
    'fuel/app/views/frontend/*/*.php',
    'fuel/app/views/frontend/*/*/*.php',
    'fuel/app/modules/**/*.php',
    'fuel/app/modules/**/**/*.php',
    'fuel/app/modules/**/**/**/*.php',
    'fuel/app/modules/**/**/**/**/*.php',
];

/*
* DEFININDO QUAIS AS VERSÕES PARA USAR O AUTOPREFIX DO CSS
*/
const autoPrefixBrowserList = ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'];

/*
* CARREGANDO AS DEPENDENCIAS USADAS PELO GULP
*/
const browserSync  = require('browser-sync');
const gulp         = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS     = require('gulp-clean-css');
const concat       = require('gulp-concat');
const plumber      = require('gulp-plumber');
const rename       = require('gulp-rename');
const sass         = require('gulp-sass');
const shell        = require('gulp-shell');
const uglify       = require('gulp-uglify');
const gutil        = require('gulp-util');

/*
* TASK - CRIANDO SERVER DE AUTORELOAD COM BROWSERSYNC
* @PROXY: URL AONDE SEU PROJETO ESTA ALOCADO
*/
gulp.task('browserSync', function() {
    browserSync({
        proxy: host,
        notify: false,
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

/*
* TASK - CONCATENANDO OS SCRIPTS
*/
gulp.task('scripts', function() {
    gulp.src(['node_modules/jquery/dist/jquery.min.js']).pipe(gulp.dest('assets/js'));
    return gulp.src(scripts)
        .pipe(plumber())
        .pipe(concat('main.js'))
        .on('error', gutil.log)
        .pipe(gulp.dest('assets/js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'));
});

/*
* TASK - COMPILANDO ARQUIVO SASS
*/
gulp.task('sass', function () {
    return gulp.src('base/sass/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('styles.css'))
        .pipe(gulp.dest('base/css'));
});

/*
* TASK - COMPILANDO O ARQUIVO CSS, COLOCANDO AUTOPREFIX PARA NAVEGADORES ANTIGOS
*/
gulp.task('styles', function () {
    return gulp.src(styles)
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(autoprefixer({
            browsers: autoPrefixBrowserList,
            cascade:  true
        }))
        .on('error', gutil.log)
        .pipe(concat('main.css'))
        .pipe(gulp.dest('assets/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS())
        .pipe(gulp.dest('assets/css'));
});

/*
* TASK - CONCATENANDO E MOVENDO O FONTAWESOME
*/
gulp.task('fontawesome', function () {
    gulp.src(['node_modules/font-awesome/fonts/*.*']).pipe(gulp.dest('assets/fonts'));
});

/*
* TASK PADRÃO PARA RODAR O GULP
* 1) RODA AS FUNÇÕES PARA CONCATENAR OS ARQUIVOS
*/
gulp.task('default', ['sass', 'styles', 'scripts', 'fontawesome'], function(){
});

/*
* TASK WATCH PARA RODAR O GULP
* 1) INICIA O WEB SERVER E O BROWSERSYNC
* 2) COMPRIME TODOS OS ARQUIVOS E GERA O ARQUIVO FINAL
*/
gulp.task('watch', ['default', 'browserSync'], function () {
    // ALTERAÇÕES NOS ARQUIVOS DO SITE
    gulp.watch('base/sass/**/*.scss', ['sass']);
    gulp.watch('base/css/*.css', ['styles', 'bs-reload']);
    gulp.watch('base/js/*.js', ['scripts', 'bs-reload']);

    // QUALQUER ALTERAÇÃO NO PROJETO, FAZ O RELOAD DA PAGINA
    gulp.watch(watchList,['bs-reload']);
});
