<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 header__title">
                <h1>Sinta-se bem com você mesmo.</h1>
                <div class="header__title__button">
                    <?php echo Html::anchor('#contato', '<strong>Dê início ao seu acompanhamento espiritual!</strong>'); ?>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="espiritual">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="espiritual__title"><strong>Acompanhamento espiritual</strong></h1>
                    <div class="row">
                        <div class="col-xs-12 col-md-offset-1 col-md-7 espiritual__text">
                            <p class="espiritual__text">
                            Descobrir e conseguir realizar seu propósito de vida pode ser uma tarefa árdua <strong> e você não precisa fazer isso sozinho. </strong> O MeuGuru te guia no processo de desenvolvimento pessoal, no qual um profissional capacitado te auxilia a superar dificuldades que impedem sua evolução.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-offset-4 col-md-7 espiritual__text">
                            <p class="text-right">
                            Através de técnicas como mindfulness, meditação e autoconhecimento, você desenvolve consciência, concentração e equilíbrio para conseguir solucionar seus problemas, minimizar sofrimentos e aumentar sua <strong>qualidade de vida.</strong>
                            </p>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-offset-1 col-md-6 espiritual__text">
                                <p class="espiritual__text">
                                Comece uma mudança de dentro para fora e desfrute de sua saúde física e mental.<br></p>
                                <strong><h4>MeuGuru é vida plena!</h4></strong>
                            </div>
                        </div>
                    </div>
                <div class="espiritual__button">
                    <?php echo Html::anchor('#contato', '<strong>Comece agora!</strong>'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="resultados">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="resultados__title">Estudos comprovam que A PRÁTICA traz resultados:</h1>
            </div>
        </div>
        <div class="resultados__plus-minus">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-1 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-4">
                    <ul class="resultados__plus-minus__lista">
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-minus resultados__plus-minus__lista__icon"></i>Insônia</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-minus resultados__plus-minus__lista__icon"></i>Estresse</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-minus resultados__plus-minus__lista__icon"></i>Ansiedade</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-minus resultados__plus-minus__lista__icon"></i>Sofrimento</li>
                    </ul>
                </div>
                <div class="col-xs-5 col-sm-3">
                    <ul class="resultados__plus-minus__lista">
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-plus resultados__plus-minus__lista__icon"></i>Empatia</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-plus resultados__plus-minus__lista__icon"></i>Atenção</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-plus resultados__plus-minus__lista__icon"></i>Equilíbrio</li>
                        <li class="resultados__plus-minus__lista__item"><i class="fas fa-plus resultados__plus-minus__lista__icon"></i>Felicidade</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="cadastre">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-offset-1 col-md-5">
                <h1 class="cadastre__title cadastre-drive">A evolução que você busca a um click de distância.</h1>
                    <div class="cadastre__button cadastre-drive">
                        <?php echo Html::anchor('#contato', '<strong>Cadastre-se agora</strong>'); ?>
                    </div>
            </div>
            <div class="col-md-6">
                <?php echo \Casset::img('cadastre_se.png', 'Homem meditando', array('class' => 'cadastre__img img-responsive')) ?>
            </div>
        </div>
    </div>
</section>
<section class="funciona" id="como-funciona">
    <div class="container">
        <div class="row">
            <h1 class="funciona__title">Como funciona</h1>
            <div class="col-xs-12 visible-xs visible-sm visible-md">
                <div class="funciona__topics">
                    <div class="row">
                        <?php echo \Casset::img('funciona_passo_1.png', 'Pessoa na tela do computador', array('class' => 'funciona__topics img-responsive')) ?>
                        <h2><strong>Faça sua avaliação</strong></h2>
                        <p>Ganhe a entrevista por videochamada.</p>
                    </div>
                    <div class="row">
                        <?php echo \Casset::img('funciona_passo_2.png', 'Pessoa meditando', array('class' => 'funciona__topics img-responsive')) ?>
                        <h2><strong>Conheça seu Guru</strong></h2>
                        <p>Indicamos o guia ideal para você.</p>
                    </div>
                    <div class="row">
                       <?php echo \Casset::img('funciona_passo_3.png', 'Fogo', array('class' => 'funciona__topics img-responsive')) ?>
                        <h2><strong>Desenvolva-se espiritualmente</strong></h2>
                        <p>Avaliamos sua evolução semestralmente.</p>
                    </div>
                    <div class="row">
                        <?php echo \Casset::img('funciona_passo_4.png', 'Coração batendo', array('class' => 'funciona__topics img-responsive')) ?>
                        <h2><strong>Alcance o estado desejado</strong></h2>
                        <p>Mude seu mindset e supere desafios.</p>
                    </div>
                </div>
            </div>
            <div class="visible-lg">
                <div class="funciona__topics">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-9">
                                    <h2><strong>Faça sua avaliação</strong></h2>
                                    <p>Ganhe a entrevista por videochamada.</p>
                                </div>
                                <div class="col-md-3 funciona__topics-left-drive__first">
                                    <?php echo \Casset::img('funciona_passo_1.png', 'Pessoa na tela do computador', array('class' => 'funciona__topics funciona__topics__img img-responsive')) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <h2><strong>Desenvolva-se espiritualmente</strong></h2>
                                    <p>Avaliamos sua evolução semestralmente.</p>
                                </div>
                                <div class="col-md-3 funciona__topics-left-drive__last">
                                    <?php echo \Casset::img('funciona_passo_3.png', 'Fogo', array('class' => 'funciona__topics funciona__topics__img img-responsive')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="funciona__topics__rope">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row funciona__topics-drive-row-first">
                                <div class="col-md-3 funciona__topics-right-drive">
                                    <?php echo \Casset::img('funciona_passo_2.png', 'Pessoa meditando', array('class' => 'funciona__topics funciona__topics__img img-responsive')) ?>
                                </div>
                                <div class="col-md-9">
                                    <h2><strong>Conheça seu Guru</strong></h2>
                                    <p>Indicamos o guia ideal para você.</p>
                                </div>
                            </div>
                            <div class="row funciona__topics-drive-row-last">
                                <div class="col-md-3 funciona__topics-right-drive">
                                    <?php echo \Casset::img('funciona_passo_4.png', 'Coração batendo', array('class' => 'funciona__topics funciona__topics__img img-responsive')) ?>
                                </div>
                                <div class="col-md-9">
                                    <h2><strong>Alcance o estado desejado</strong></h2>
                                    <p>Mude seu mindset e supere desafios!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="funciona__button">
            <?php echo Html::anchor('#contato', '<strong>Cadastre-se agora!</strong>'); ?>
        </div>
    </div>
</section>
<section class="atendimentos" id="atendimento">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="atendimentos__title"><strong>Atendimentos</strong></h1>
                <p class="atendimentos__text">
                Fale com seu guru sempre que precisar, sem ter que sair do lugar.<br>
                Com o MeuGuru você garante suporte/atendimento:
                </p>
            </div>
        </div>
        <div class="row atendimentos__topics">
            <div class="col-xs-12">
                <h2 class="atendimentos__topics__item">
                <?php echo \Casset::img('check-icon.png', 'Icone de checado', array('class' => 'atendimentos__topics__item__icon img-responsive')) ?>
                <strong>Prático</strong></h2>
                <h2 class="atendimentos__topics__item">
                <?php echo \Casset::img('check-icon.png', 'Icone de checado', array('class' => 'atendimentos__topics__item__icon img-responsive')) ?>
                <strong>Confidencial</strong></h2>
                <h2 class="atendimentos__topics__item">
                <?php echo \Casset::img('check-icon.png', 'Icone de checado', array('class' => 'atendimentos__topics__item__icon img-responsive')) ?>
                <strong>Acessível</strong></h2>
            </div>
            <div class="col-xs-12">
                <h2 class="atendimentos__topics__item">
                <?php echo \Casset::img('check-icon.png', 'Icone de checado', array('class' => 'atendimentos__topics__item__icon img-responsive')) ?>
                <strong>Efetivo</strong></h2>
                <h2 class="atendimentos__topics__item">
                <?php echo \Casset::img('check-icon.png', 'Icone de checado', array('class' => 'atendimentos__topics__item__icon img-responsive')) ?>
                <strong>Integral</strong></h2>
            </div>
        </div>
    </div>
</section>
<section class="planos">
    <h1 class="planos__title"><strong>Planos</strong></h1>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="planos-right planos__card planos__card__gray">
                        <?php echo \Casset::img('planos_card1.png', 'Foto de mulher com celular na mão', array('class' => 'img-responsive planos__card__img')); array('style' => 'width:100%') ?>
                        <div class="planos__card__text">
                            <h2><strong>Chat ilimitado</strong></h2>
                            <p>Você realiza quantos
                            atendimentos forem necessários
                            e a cada 6 meses você se encontra
                            com seu Guru espiritual por
                            videoconferência para reavaliações</p>
                            <h3><strong>R$<span class="planos__card__bigger-text">189</span>,00<span class="planos__card__smaller-text">/mês</span></strong></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="planos-left planos__card">
                        <?php echo \Casset::img('planos_card2.png', 'Foto de mulher sorrindo', array('class' => 'img-responsive planos__card__img')); array('style' => 'width:100%') ?>
                        <div class="planos__card__text">
                            <h2><strong>Atendimento avulso</strong></h2>
                            <p>Caso precise se encontrar mais
                            vezes com seu Guru, você pode
                            agendar uma videoconferência
                            avulsa para que ele te ajude
                            em questões pontuais.</p>
                            <h3><strong>R$<span class="planos__card__bigger-text">80</span>,00</strong></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="acompanhamento">
    <div class="container">
        <div class="row">
            <div class="col-xs-7">
                <div class="acompanhamento-drive acompanhamento__button">  
                    <?php echo Html::anchor('#contato', '<strong>Comece seu acompanhamento espiritual agora!<strong>', array('class' => 'acompanhamento-drive')); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="formulario" id="contato">
    <div class="container">
        <h2 class="formulario__title"><strong>Entre em contato conosco!</strong></h2>
        <form class="" method="POST" action="#" accept-charset="utf-8">
            <input class="formulario__fields" placeholder="* Nome" name="nome" value="" type="text"/>
            <input class="formulario__fields" placeholder="* Sobrenome" name="sobrenome" value="" type="text"/>	
            <input class="formulario__fields" placeholder="* Telefone" name="telefone" value="" type="text"/>				
            <input class="formulario__fields" placeholder="* E-mail" name="email" value="" type="text"/>				
            <textarea class="formulario__fields formulario__fields__text-area" placeholder="* Mensagem" rows="8" name="mensagem"></textarea>
            <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="formulario__button">
                <strong>Enviar</strong>
            </button>
            </div>
        </form>
    </div>
</section>