<?php
return array(
	'paths' => array(
		'core' => 'assets/',
		'fonts' => array(
			'path' => 'assets/fonts/',
			'css_dir' => '',
		),
		'upload' => array(
		    'path' => 'assets/upload/',
		    'js_dir' => '',
		    'css_dir' => '',
		    'img_dir' => '',
        ),
        'placeholdit.co' => array(
            'path' => 'https://placeholdit.co//i/',
            'js_dir' => '',
            'css_dir' => '',
            'img_dir' => '',
        ),
        'yahooapis' => array(
            'path' => '//bp.yahooapis.com/',
		    'js_dir' => '',
		    'css_dir' => '',
		    'img_dir' => '',
        ),
        'googlefonts' => array(
			'path' => 'https://fonts.googleapis.com/css?family=',
			'css_dir' => ''
		),
		'youtube' => array(
			'path' => 'http://img.youtube.com/vi/',
			'js_dir' => '',
			'css_dir' => '',
			'img_dir' => '',
		),
        'lib' => array(
            'path' => 'assets/lib/',
		    'js_dir' => '',
		    'css_dir' => '',
		    'img_dir' => '',
        ),
        'controle' => 'assets/controle/',
        'controlelib' => array(
            'path' => 'assets/controle/lib/',
            'js_dir' => '',
            'css_dir' => '',
            'img_dir' => '',
        ),
        'controleupload' => array(
            'path' => 'assets/controle/upload/',
            'img_dir' => ''
        ),
	),

    'min' => (\Fuel::$env == \Fuel::PRODUCTION),
    'combine' => (\Fuel::$env == \Fuel::PRODUCTION),
    'show_files' => false,
    'show_files_inline' => false,
);
