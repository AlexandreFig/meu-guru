<?php echo Form::open(array("class" => "form-horizontal", 'enctype' => 'multipart/form-data')); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Dados do Sistema</h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<?php echo Form::label('Título', 'titulo', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::input('titulo', Input::post('titulo', $registro->titulo), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('Telefone', 'telefone', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::input('telefone', Input::post('telefone', $registro->telefone), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('Celular', 'celular', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::input('celular', Input::post('celular', $registro->celular), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('E-mail', 'email', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::input('email', Input::post('email', $registro->email), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('Endereço', 'endereco', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::textarea('endereco', Input::post('endereco', $registro->endereco), array('class' => 'form-control')); ?>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Redes Sociais</h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-facebook" aria-hidden="true"></i></span>
					<?php echo Form::input('facebook', Input::post('facebook', $registro->facebook), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
					<?php echo Form::input('google_plus', Input::post('google_plus', $registro->google_plus), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-instagram" aria-hidden="true"></i></span>
					<?php echo Form::input('instagram', Input::post('instagram', $registro->instagram), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-linkedin" aria-hidden="true"></i></span>
					<?php echo Form::input('linkedin', Input::post('linkedin', $registro->linkedin), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-pinterest" aria-hidden="true"></i></span>
					<?php echo Form::input('pinterest', Input::post('pinterest', $registro->pinterest), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-twitter" aria-hidden="true"></i></span>
					<?php echo Form::input('twitter', Input::post('twitter', $registro->twitter), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-youtube" aria-hidden="true"></i></span>
					<?php echo Form::input('youtube', Input::post('youtube', $registro->youtube), array('class' => 'form-control')); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Configurações de SEO e OpenGraph do Facebook</h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<?php echo Form::label('Título', 'meta_title', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-5">
				<?php echo Form::input('meta_title', Input::post('meta_title', $registro->meta_title), array('class' => 'form-control meta_title', 'maxlength' => '120')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('Descrição', 'meta_description', array('class' => 'col-sm-3 control-label', 'maxlength' => '155')); ?>

			<div class="col-sm-9">
				<?php echo Form::textarea('meta_description', Input::post('meta_description', $registro->meta_description), array('class' => 'form-control', 'maxlength' => '155')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('Palavras Chaves', 'meta_keywords', array('class' => 'col-sm-3 control-label')); ?>

			<div class="col-sm-9">
				<?php echo Form::input('meta_keywords', Input::post('meta_keywords', $registro->meta_keywords), array('class' => 'form-control')); ?>
				<span class="help-block"><small>Use vírgula para separar as palavras-chave.</small></span>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Extra</h3>
    </div>
    <div class="panel-body" style="padding: 15px;">
        <div class="form-group">
            <label for="conteudo_extra_texto">Javascript Extra</label>
            <?php echo Form::hidden('conteudo_extra_texto', $registro->conteudo_extra_texto); ?>
            <div class="ace-editor" id="conteudo_extra_texto"><?php echo $registro->conteudo_extra_texto; ?></div>
        </div>
        <div class="form-group">
            <label for="conteudo_extra_css">CSS Extra</label>
            <?php echo Form::hidden('conteudo_extra_css', $registro->conteudo_extra_css); ?>
            <div class="ace-editor" id="conteudo_extra_css"><?php echo trim($registro->conteudo_extra_css); ?></div>
        </div>
    </div>
</div>

<div class="btn-save-container">
    <div class="btn-back pull-left">
        <?php echo Html::anchor(Uri::back(), '<span class="glyphicon glyphicon-chevron-left"></span> Voltar', array('class' => 'btn btn-primary')); ?>
    </div>
    <div class="btn-save pull-right">
        <button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-ok"></span>
            Salvar <?php echo $titulo['singular']; ?></button>
    </div>
    <div class="clearfix"></div>
</div>

<?php echo Form::close(); ?>

<script src="/assets/controle/lib/ace/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="/assets/controle/lib/ace/src-min-noconflict/theme-monokai.js" type="text/javascript" charset="utf-8"></script>
<script src="/assets/controle/lib/ace/src-min-noconflict/mode-javascript.js" type="text/javascript" charset="utf-8"></script>
<script src="/assets/controle/lib/ace/src-min-noconflict/mode-css.js" type="text/javascript" charset="utf-8"></script>

<script>
    var conteudo_extra_texto = ace.edit("conteudo_extra_texto");
    conteudo_extra_texto.setTheme("ace/theme/monokai");
    conteudo_extra_texto.getSession().setMode("ace/mode/javascript");
    conteudo_extra_texto.setAutoScrollEditorIntoView();
    conteudo_extra_texto.getSession().setUseWrapMode(true);

    var conteudo_extra_css = ace.edit("conteudo_extra_css");
    conteudo_extra_css.setTheme("ace/theme/monokai");
    conteudo_extra_css.getSession().setMode("ace/mode/css");
    conteudo_extra_css.getSession().setUseWrapMode(true);
</script>

<?php
$js_inline = "
$(\".btn-save button\").click(function()
    {	
        $(\"input[name=conteudo_extra_texto]\").val(conteudo_extra_texto.getSession().getValue());
        $(\"input[name=conteudo_extra_css]\").val(conteudo_extra_css.getSession().getValue());
    });
";
Casset::js_inline($js_inline);
?>
