<?php foreach ($registros as $registro) { ?>
    <tr data-id="<?php echo $registro->id; ?>">
        <td class="text-center rowlink-skip">
            <input type="checkbox" name="registros[]" class="td_checkbox" value="<?php echo $registro->id; ?>" />
        </td>
        <td>
            <strong><?php echo $registro->tipo(); ?></strong>
        </td>
        <td>
            <a href="<?php echo \Uri::controller('/visualizar/'.$registro->id); ?>" title="Visualizar este item">
                <?php echo $registro->nome; ?>
            </a>
        </td>
        <td>
            <strong><?php echo $registro->email; ?></strong>
        </td>
        <td>
            <strong><?php echo $registro->created_at(); ?></strong>
        </td>
    </tr>
<?php } ?>
