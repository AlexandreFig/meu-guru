<?php

class Validation extends \Fuel\Core\Validation
{
	/**
	 * Verifica se o CPF é válido
	 * @param  string $valor
	 * @return bool
	 */
	public static function _validation_valid_cpf($valor)
	{
		Validation::active()->set_message('valid_cpf', 'O campo ":label" não é valido.');

		if (empty($valor)) {
			return true;
		}

		$j = 0;
		$num = array();
		for ($i = 0; $i < (strlen($valor)); $i++) {
			if (is_numeric($valor[$i])) {
				$num[$j] = $valor[$i];
				$j++;
			}
		}

		if (count($num) != 11) {
			$cpf_valido = false;
		} else {
			for ($i = 0; $i < 10; $i++) {
				if ($num[0] == $i and $num[1] == $i and $num[2] == $i and
					$num[3] == $i and $num[4] == $i and $num[5] == $i and
					$num[6] == $i and $num[7] == $i and $num[8] == $i) {
					$cpf_valido = false;
					break;
				}
			}
		}

		// Digito verificador 1
		if (!isset($cpf_valido)) {
			$j = 10;
			for ($i = 0; $i < 9; $i++) {
				$multiplica[$i] = $num[$i] * $j;
				$j--;
			}
			$soma = array_sum($multiplica);
			$resto = $soma % 11;
			if ($resto < 2) {
				$dg = 0;
			} else {
				$dg = 11 - $resto;
			}
			if ($dg != $num[9]) {
				$cpf_valido = false;
			}
		}

		// Digito verificador 2
		if (!isset($cpf_valido)) {
			$j = 11;
			for ($i = 0; $i < 10; $i++) {
				$multiplica[$i] = $num[$i] * $j;
				$j--;
			}
			$soma = array_sum($multiplica);
			$resto = $soma % 11;
			if ($resto < 2) {
				$dg = 0;
			} else {
				$dg = 11 - $resto;
			}
			if ($dg != $num[10]) {
				$cpf_valido = false;
			} else {
				$cpf_valido = true;
			}
		}
		return $cpf_valido;
	}

	/**
	 * Verifica se o CNPJ é válido
	 * @param  string $valor
	 * @return bool
	 */
	public static function _validation_valid_cnpj($valor)
	{
		Validation::active()->set_message('valid_cnpj', 'O campo ":label" não é valido.');

		if (empty($valor)) {
			return true;
		}

		$valor = preg_replace('/[^0-9]/', '', (string)$valor);
		// Valida tamanho
		if (strlen($valor) != 14)
			return false;
		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
			$soma += $valor{$i} * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}
		$resto = $soma % 11;
		if ($valor{12} != ($resto < 2 ? 0 : 11 - $resto))
			return false;
		// Valida segundo dígito verificador
		for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
			$soma += $valor{$i} * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}
		$resto = $soma % 11;
		return $valor{13} == ($resto < 2 ? 0 : 11 - $resto);
	}

	public function _validation_max_length_html($valor, $length = 0)
	{
		if (\Validation::active())
			\Validation::active()->set_message('max_length_html', 'O campo ":label" não deve conter mais que :param:1 caracteres.');

		$valor = html_entity_decode($valor);
		$valor = strip_tags($valor);
		$valor = preg_replace('/\s/', '', $valor);

		return (mb_strlen($valor) <= $length);
	}

	public static function _validation_real_min($valor, $min)
	{
		\Validation::active()->set_message('real_min', 'O valor mínimo do campo ":label" deve ser :param:1.');

		if ($valor != '') {
			$valor = \Funcoes::real2dolar($valor);
			return ($valor >= $min);
		}
		return true;
	}

	public static function _validation_real_max($valor, $min)
	{
		\Validation::active()->set_message('real_max', 'O valor máximo do campo ":label" deve ser :param:1.');

		if ($valor != '') {
			$valor = \Funcoes::real2dolar($valor);
			return ($valor <= $min);
		}
		return true;
	}

	/**
	 * Verifica se o campo é único
	 * @param  string $valor
	 * @param  string $tabela
	 * @param  string $campo
	 * @param  int $id
	 * @param  int $fk_site
	 * @return bool
	 */
	public static function _validation_unique($valor, $tabela, $campo, $id = 0, $campo_id = 'id')
	{
		if ($valor != '')
		{
			Validation::active()->set_message('unique', 'O :label <strong>:value</strong> já está sendo utilizado.');

			$query = DB::select(DB::expr('count(*) as qtde'))->from(Str::lower($tabela))
				->where($campo, '=', Str::lower($valor))
				->where($campo_id, '<>', $id);

			$resultado = $query->execute()->as_array();
			return ! ($resultado[0]['qtde'] > 0);
		}
		return true;
	}

	public static function _validation_required_with_value($valor, $campo, $campo_valor)
	{
		\Validation::active()->set_message('required_with_value', 'O campo ":label" é obrigatório e deve conter algum valor.');

		if (\Input::post($campo) == $campo_valor and empty($valor)) {
			return false;
		}
		return true;
	}

	public static function _validation_required_with_not_value($valor, $campo, $campo_valor)
	{
		\Validation::active()->set_message('required_with_not_value', 'O campo ":label" é obrigatório e deve conter algum valor.');

		if (\Input::post($campo) != $campo_valor and empty($valor)) {
			return false;
		}
		return true;
	}


	public function _validation_date_min($val, $format = null, $min, $min_format = null)
	{
		\Validation::active()->set_message('date_min', 'O campo ":label" deve igual ou maior que :param:2.');

		if ($this->_empty($val)) {
			return true;
		}

		/* Valor Input */
		if ($format) {
			$parsed = date_parse_from_format($format, $val);
		} else {
			$parsed = date_parse($val);
		}

		if (\Arr::get($parsed, 'error_count', 1) + (\Arr::get($parsed, 'warning_count', 1)) === 0) {
			$date = date('Y-m-d H:i:s', mktime($parsed['hour'] ?: 0, $parsed['minute'] ?: 0, $parsed['second'] ?: 0, $parsed['month'] ?: 1, $parsed['day'] ?: 1, $parsed['year'] ?: 1970));
		} else {
			return false;
		}

		/* Valor Min */
		if ($min_format) {
			$parsed = date_parse_from_format($min_format, $min);
		} else {
			$parsed = date_parse($min);
		}

		if (\Arr::get($parsed, 'error_count', 1) + (\Arr::get($parsed, 'warning_count', 1)) === 0) {
			$date_min = date('Y-m-d H:i:s', mktime($parsed['hour'] ?: 0, $parsed['minute'] ?: 0, $parsed['second'] ?: 0, $parsed['month'] ?: 1, $parsed['day'] ?: 1, $parsed['year'] ?: 1970));
		} else {
			return false;
		}

		/* Retorna se está valido ou não */
		return $date >= $date_min;
	}

	public function _validation_array_between($val, $min_val, $max_val)
	{
		\Validation::active()->set_message('array_between', 'O campo ":label" deve conter entre :param:1 e :param:2 selecionado(s).');

		return $this->_empty($val) or (count($val) >= floatval($min_val) and count($val) <= floatval($max_val));
	}

	public static function _validation_recaptcha($valor)
	{
		if (empty($valor)) {
			return false;
		}
		$valor = \Input::post('g-recaptcha-response', array('success' => false, 'error-codes' => 'missing-input'));

		/* recapatcha */
		$sessao_curl = curl_init();
		curl_setopt($sessao_curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($sessao_curl, CURLOPT_FAILONERROR, true);

		//  CURLOPT_CONNECTTIMEOUT
		//  o tempo em segundos de espera para obter uma conexão
		curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);

		//  CURLOPT_TIMEOUT
		//  o tempo máximo em segundos de espera para a execução da requisição (curl_exec)
		curl_setopt($sessao_curl, CURLOPT_TIMEOUT, 40);

		//  CURLOPT_RETURNTRANSFER
		//  TRUE para curl_exec retornar uma string de resultado em caso de sucesso, ao
		//  invés de imprimir o resultado na tela. Retorna FALSE se há problemas na requisição
		curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($sessao_curl, CURLOPT_POST, true);
		curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, array('secret' => \Config::get('site.recaptcha.' . \Fuel::$env . '.secretkey'), 'response' => $valor));
		$responses = curl_exec($sessao_curl);
		curl_close($sessao_curl);
		$responses = json_decode($responses, true);

		return (isset($responses['success']) and $responses['success'] == true);

	}
}
